package com.FPPDictionary;

import java.util.ArrayList;

public class XMLData {

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public void setID(String id)
	{
		this.id = id;
	}

	public String getID()
	{
		return this.id;
	}

	private String word;
	private String id;

	public ArrayList<Difinition>difinition = new ArrayList<Difinition>();

	public class Difinition {
		public String partsOfspeech;
		public ArrayList<Meaning>meaning = new ArrayList<Meaning>();
	}

	public class Meaning {
		String content;
		String example;
		String synonym;
	}


	public String toString()
	{
		String output = new String();
		output += word + "\n";
		
		for(int i = 0; i < difinition.size(); i++)
		{
			output += "\t" + difinition.get(i).partsOfspeech + "\n";
 
			for(int j = 0; j < difinition.get(i).meaning.size(); j++)
			{
				if(!difinition.get(i).meaning.get(j).content.equals(""))
					output +=  "\t" + difinition.get(i).meaning.get(j).content + "\n";
				if(!difinition.get(i).meaning.get(j).example.equals(""))
					output +=  "\tEg. " + difinition.get(i).meaning.get(j).example + "\n";
				if(!difinition.get(i).meaning.get(j).synonym.equals(""))
					output +=  "\tSynonyms: " + difinition.get(i).meaning.get(j).synonym + "\n";
				
				output += "\n";
			}
		

		}
		return output;
	}
}
