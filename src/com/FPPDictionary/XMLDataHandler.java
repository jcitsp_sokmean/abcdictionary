package com.FPPDictionary;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

public class XMLDataHandler {

	public static final String FILE_URI = "input.xml";

	private static Document getDocument()
	{
		Document document = null; 
		try
		{
			File inputFile = new File(FILE_URI);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			document = dBuilder.parse(inputFile);
			document.getDocumentElement().normalize();

		}catch(Exception e){
			e.printStackTrace();
		}
		return document;
	}

	
	

	// This method is for create a new node with providing data
	private Element creatNode (XMLData data,Document doc) throws Exception 
	{

		Node dictionary = doc.getElementsByTagName("dictionary").item(0);
		
		Element wordData  = doc.createElement("wordData");
		Element word = doc.createElement("word");
		
		wordData.setAttribute("id",data.getID());
		word.setTextContent(data.getWord());

		wordData.appendChild(word);
		dictionary.appendChild(wordData);

		for(XMLData.Difinition dif : data.difinition)
		{
			Element difinition = doc.createElement("difinition");
			Element partOfSpeach = doc.createElement("partOfSpeach");
			Element meaningElement = doc.createElement("meaning");
			
			partOfSpeach.setTextContent(dif.partsOfspeech);
			difinition.appendChild(partOfSpeach);

			for(XMLData.Meaning meaning : dif.meaning)
			{
				if(meaning.content != null && meaning.content != null)
				{
					Element eMeaning = doc.createElement("content");
					eMeaning.setTextContent(meaning.content);
					meaningElement.appendChild(eMeaning);
				}
				if(meaning.example != null && meaning.example != "")
				{
					Element example = doc.createElement("example");
					example.setTextContent(meaning.example);
					meaningElement.appendChild(example);
				}

				if(meaning.synonym != null && meaning.synonym != "")
				{
					Element synonym = doc.createElement("synonyms");
					synonym.setTextContent(meaning.synonym);
					meaningElement.appendChild(synonym);
				}
			}
			difinition.appendChild(meaningElement);
			wordData.appendChild(difinition);
		}
		
		return wordData;

	}
	
	public String toString()
	{
		XMLData[] arrData = dataLoader();
		String output = new String();
		for(int i = 0; i < arrData.length; i++)
		{
			output += arrData[i].getID()+ ":" +arrData[i].getWord() +"\n";

			for (XMLData.Difinition dif : arrData[i].difinition)
			{
				output += "\t" + dif.partsOfspeech + "\n";

				for(XMLData.Meaning meaning : dif.meaning)
				{
					output += "\t" +meaning.content + "\n";
					output += "\t" +meaning.example + "\n";
					output += "\t" +meaning.synonym + "\n";
				}	
			}
		}
		return output;
	}
	
	private void saveDoc(Document doc) throws Exception
	{
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT,"yes");

		StreamResult result = new StreamResult(new FileWriter("input.xml"));
		DOMSource source = new DOMSource(doc);    
		transformer.transform(source, result);
	}
	
	private int getLastNodeId()
	{
		Document doc = getDocument();
		
		NodeList wordList = doc.getElementsByTagName("wordData");
		
		NamedNodeMap attrNode = wordList.item(wordList.getLength() - 1).getAttributes();
		
		Node node = attrNode.getNamedItem("id");
		
		return Integer.parseInt(node.getTextContent()) + 1;
	}
	
	public XMLData[] dataLoader() 
	{
		XMLData[] data = null;

		NodeList list = getDocument().getElementsByTagName("wordData");

		data = new XMLData[list.getLength()];

		for (int temp = 0; temp < list.getLength(); temp++) 
		{
			
			XMLData xmldata = new XMLData();
			Element eElement = (Element) list.item(temp);
			
			Node word = eElement.getElementsByTagName("word").item(0);
			NamedNodeMap attr = eElement.getAttributes();
			
			Node nodeAttr = attr.getNamedItem("id");
			NodeList difinitionList = eElement.getElementsByTagName("difinition");

			for(int i = 0 ; i < difinitionList.getLength() ; i++)
			{
				Element dElement = (Element)difinitionList.item(i);

				Node dNode = dElement.getElementsByTagName("partOfSpeach").item(0);

				NodeList meaningList = dElement.getElementsByTagName("meaning");

				XMLData.Difinition difinitionObj =  xmldata.new Difinition();

				for(int j = 0; j < meaningList.getLength(); j++)
				{
					XMLData.Meaning meaningObj = xmldata.new Meaning();

					Element cEml = (Element)meaningList.item(j);

					Node cNode = cEml.getElementsByTagName("content").item(0);
					Node eNode = cEml.getElementsByTagName("example").item(0);
					Node sNode = cEml.getElementsByTagName("synonyms").item(0);

					if(cNode != null)
					{
						meaningObj.content = cNode.getTextContent();
					}

					String eg = (eNode == null || eNode.getTextContent() == null) ? "": eNode.getTextContent();
					String syn = (sNode == null || sNode.getTextContent() == null) ? "": sNode.getTextContent();

					//if(eNode != null)
					{
						meaningObj.example = eg;
					}
					//if(sNode != null)
					{
						meaningObj.synonym = syn;
					}

					if(meaningList != null)
						difinitionObj.meaning.add(meaningObj);

				}

				if(dNode == null)
					difinitionObj.partsOfspeech = "";
				else
					difinitionObj.partsOfspeech = dNode.getTextContent();

				if(difinitionObj != null)
					xmldata.difinition.add(difinitionObj);

			}
			if(nodeAttr != null)
			{
				xmldata.setID(nodeAttr.getTextContent());
			}
			if(word != null){
				xmldata.setWord(word.getTextContent());
			}
			data[temp] = xmldata;

		}

		return data; 

	}
	
	public void update(String nodeId, XMLData newData) throws Exception
	{
		Document doc = getDocument();
		
		NodeList wordList = doc.getElementsByTagName("wordData");
		
		int index = 0;
		
		while(index < wordList.getLength())
		{
			NamedNodeMap attrNode = wordList.item(index).getAttributes();
			Node node = attrNode.getNamedItem("id");
			
			if(node.getTextContent().equals(nodeId))
			{
				
				Element parent = (Element)doc.getElementsByTagName("dictionary").item(0);
				
				Node replaceNode = wordList.item(index);
				
				Element newElement = creatNode(newData, doc);
				
				replaceNode.getParentNode().replaceChild(newElement, replaceNode);
				
				saveDoc(doc);
				
				System.out.println("Record was successfully updated");
				return;
			}
			index++;
		}
		
	}

	public void addNewNode(XMLData data) throws Exception
	{
		Document doc = getDocument();
		
		Node dictionary = doc.getElementsByTagName("dictionary").item(0);
		
		Element wordData  = doc.createElement("wordData");
		
		Element word = doc.createElement("word");
		
		wordData.setAttribute("id", Integer.toString(getLastNodeId()));
		
		word.setTextContent(data.getWord());

		wordData.appendChild(word);
		
		dictionary.appendChild(wordData);

		for(XMLData.Difinition dif : data.difinition)
		{
			Element difinition = doc.createElement("difinition");
			Element partOfSpeach = doc.createElement("partOfSpeach");
			Element meaningElement = doc.createElement("meaning");
			
			partOfSpeach.setTextContent(dif.partsOfspeech);
			difinition.appendChild(partOfSpeach);

			for(XMLData.Meaning meaning : dif.meaning)
			{
				if(meaning.content != null && meaning.content != null)
				{
					Element eMeaning = doc.createElement("content");
					eMeaning.setTextContent(meaning.content);
					meaningElement.appendChild(eMeaning);
				}
				if(meaning.example != null && meaning.example != "")
				{
					Element example = doc.createElement("example");
					example.setTextContent(meaning.example);
					meaningElement.appendChild(example);
				}

				if(meaning.synonym != null && meaning.synonym != "")
				{
					Element synonym = doc.createElement("synonyms");
					synonym.setTextContent(meaning.synonym);
					meaningElement.appendChild(synonym);
				}
			}
			difinition.appendChild(meaningElement);
			wordData.appendChild(difinition);
		}
		saveDoc(doc);
		
	}
	
	public void deleteNode(String nodeId) throws Exception
	{
		Document doc = getDocument();
		
		NodeList wordList = doc.getElementsByTagName("wordData");
		
		int index = 0;
		
		while(index < wordList.getLength())
		{
			NamedNodeMap attrNode = wordList.item(index).getAttributes();
			Node node = attrNode.getNamedItem("id");
			
			if(node.getTextContent().equals(nodeId))
			{
				Element parent = (Element)doc.getElementsByTagName("dictionary").item(0);
				
				parent.removeChild(wordList.item(index));
				
				saveDoc(doc);
				
				System.out.println("Record " + nodeId + " was deleted");
				return;
			}
			index++;
		}
		
	}
	
	public ArrayList getWordList()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		XMLData[] objList = Arrays.copyOf(dataLoader(),dataLoader().length);
		
		for(int i = 0; i < dataLoader().length; i++)
		{
			list.add(objList[i].getWord());
		}
		
		return list;
	}
	
	public XMLData getDifinition(String word)
	{
		XMLData[] objList = Arrays.copyOf(dataLoader(),dataLoader().length);
		
		for(int i = 0; i < dataLoader().length; i++)
		{
			if(objList[i].getWord().equalsIgnoreCase(word))
			{
				//System.out.println(objList[i].toString());
				return objList[i];
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) throws Exception{

		XMLDataHandler handler = new XMLDataHandler();
		
		// Search by word
		System.out.println(handler.getDifinition("Intuition").toString());
		
		//handler.getWordList();
		
		// Usage retrieve all record from xml file as array of XMLData object
		// handler.dataLoader()
		
		// Usage : update method
		// handler.update("3",handler.dataLoader()[0]);
		
		// Usage : add new node to last index
		// handler.addNewNode(handler.dataLoader()[0]);
		
		// Usage : delete a node by id
		// handler.deleteNode("5");
	}
}
